package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
)

var Viewers sync.Map

var cstZone = time.FixedZone("UTC+8", 8*3600)

type WebsoketViwers struct {
	WebsoketViwers []Viewer `json:"WebsocketViewer"`
}

type Viewer struct {
	RemoteAddr string `json:"RemoteAddr"`
	Channel    string `json:"Channel"`
	UserAgent  string `json:"UserAgent"`
	StartTime  int64  `json:"StartTime"`
	EndTime    int64  `json:"EndTime"`
	WatchTime  string `json:"WatchTime"`
}

func (p WebsoketViwers) Len() int { return len(p.WebsoketViwers) }

func (p WebsoketViwers) Less(i, j int) bool {
	return p.WebsoketViwers[i].StartTime >= p.WebsoketViwers[j].StartTime
}

func (p WebsoketViwers) Swap(i, j int) {
	p.WebsoketViwers[i], p.WebsoketViwers[j] = p.WebsoketViwers[j], p.WebsoketViwers[i]
}

func WebSocketGetViewers() *WebsoketViwers {

	Websoket := new(WebsoketViwers)
	Viewers.Range(func(key, value interface{}) bool {
		//log.Println(key, value)
		add := value.(Viewer)
		watchTime := time.Now().In(cstZone).Unix() - add.StartTime

		add.WatchTime = SecondsToTimestamp(watchTime)
		Websoket.WebsoketViwers = append(Websoket.WebsoketViwers, add)
		return true
	})
	sort.Sort(Websoket)
	return Websoket
}

func SecondsToTimestamp(time int64) string {
	var hour, min, sec string
	tmp := time
	if tmp >= 3600 {
		tmp = tmp / 3600
		hour = strconv.Itoa(int(tmp))
	} else {
		hour = strconv.Itoa(int(0))
	}

	time %= 3600
	tmp = time
	if tmp >= 60 {
		tmp = tmp / 60
		min = strconv.Itoa(int(tmp))
	} else {
		min = strconv.Itoa(int(0))
	}

	time %= 60
	sec = strconv.Itoa(int(time))

	if hour == "0" && min == "0" {
		return sec + "s"
	}
	if hour == "0" {
		return min + "m " + sec + "s"
	}
	return hour + "h " + min + "m " + sec + "s"
}

func WriteAccessLog(c *gin.Context) {
	if Config.AccessLog {
		// Format : timestampe , ClientIP , HttpMethod, Path , HttpProtocal , UserAgent
		WriteLog(c.ClientIP() + " [" + time.Now().In(cstZone).Format("02/Jan/2006:15:04:05 MST") + "] " + " " + c.Request.Method + " \"" + c.Request.URL.Path + " " + c.Request.Proto + "\" \"" + c.Request.UserAgent() + "\"")
	}
}

func WriteLeaveLog(tmp Viewer) {
	if Config.PlayerLog {
		// Format : timestampe , ClientIP , WatchChannel , StartWatchTime , EndWatchTime , WatchTime
		WriteLog(tmp.RemoteAddr + " [" + time.Now().In(cstZone).Format("02/Jan/2006:15:04:05 MST") + "] " + " LEAVE \"" + tmp.Channel + "\"  " + time.Unix(tmp.StartTime, 0).Format("02/Jan/2006:03:04:05") + "  " + time.Unix(time.Now().In(cstZone).Unix(), 0).Format("02/Jan/2006:03:04:05") + " (" + SecondsToTimestamp(time.Now().In(cstZone).Unix()-tmp.StartTime) + ")")
	}
}

func PleyerLog() {
	if Config.PlayerLog {
		// Format : ClientIP , Channel , StartWatchTime. Record every five seconds
		for {
			Viewers.Range(func(key, value interface{}) bool {
				tmp := value.(Viewer)
				WriteLog(tmp.RemoteAddr + " [" + time.Now().In(cstZone).Format("02/Jan/2006:15:04:05 MST") + "] " + " PLAY \"" + tmp.Channel + "\" (" + SecondsToTimestamp(time.Now().In(cstZone).Unix()-tmp.StartTime) + ")")
				return true
			})
			time.Sleep(time.Second * 5)
		}
	}
}

func WriteLog(w string) {

	f, err := os.OpenFile("access.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0777)
	if err != nil {
		fmt.Println("os OpenFile error: ", err)
		return
	}
	defer f.Close()
	f.WriteString(w + "\n")
}
