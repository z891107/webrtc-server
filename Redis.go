package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/go-redis/redis"
)

var PublicIP = GetPublicIP()

var (
	_Addr     = Config.RedisST.Address
	_Password = Config.RedisST.Password
	_DB       = 0
)

// Error Status :	0	OK
// 					1	Token not exist
//					2 	Redis Ping Error
//					3	Query Error
func Get_String(Token string) int {
	// connect server
	client := redis.NewClient(&redis.Options{
		Addr:     _Addr,
		Password: _Password,
		DB:       _DB,
	})

	defer client.Close()

	// check server service
	_, err := client.Ping().Result()
	if err != nil {
		log.Println("ping error (Get)", err.Error())
		return 2
	}
	_, err = client.Get(Token).Result()

	if err == redis.Nil {
		// Token not exist
		log.Println("Token not exist (Get)")
		return 1
	} else if err != nil {
		// Error
		log.Print(err)
		return 3
	} else {
		// Token exist
		log.Println("Token exist (Get)")
		return 0
	}

}

// Error Status :	0	OK
// 					1	Token not exist
//					2 	Redis Ping Error
//					3	Query Error
func Delete_String(Token string) {
	// connect server
	client := redis.NewClient(&redis.Options{
		Addr:     _Addr,
		Password: _Password,
		DB:       _DB,
	})

	defer client.Close()

	// check server service
	_, err := client.Ping().Result()
	if err != nil {
		log.Println("ping error (Delete)", err.Error())
		return
	}
	_, err = client.Del(Token).Result()

	if err == redis.Nil {
		// Token not exist
		log.Println("Token not exist (Delete)")
	} else if err != nil {
		// Error
		log.Print(err)
	} else {
		// Token exist
		log.Println("Token exist (Delete)")
	}
}

func Get_ServerList() *ServerList {
	// connect server
	client := redis.NewClient(&redis.Options{
		Addr:     _Addr,
		Password: _Password,
		DB:       _DB,
	})

	defer client.Close()

	// check server service
	_, err := client.Ping().Result()
	if err != nil {
		log.Println("ping error", err.Error())
		return nil
	}

	var cursor uint64
	for {
		var keys []string
		var err error
		var match string = "ServerIP_*"
		var count int64 = 0 // count = 0 is all
		keys, cursor, err = client.Scan(cursor, match, count).Result()
		if err != nil {
			log.Print(err)
		}

		if cursor == 0 { // no more keys
			result := new(ServerList)
			result.ServerList = keys

			return result
		}
	}
}

func Set_String(IP string) {
	// connect server
	client := redis.NewClient(&redis.Options{
		Addr:     _Addr,
		Password: _Password,
		DB:       _DB,
	})

	defer client.Close()

	// check server service
	_, err := client.Ping().Result()
	if err != nil {
		log.Println("ping error", err.Error())
		return
	}

	err = client.Set("ServerIP_"+IP, IP, time.Second*60).Err()
	if err != nil {
		log.Print(err)
	}
}

func HealCheck() {
	for {
		Set_String(PublicIP)
		time.Sleep(time.Second * 60)
	}
}

func GetPublicIP() string {

	responseClient, errClient := http.Get("https://ipw.cn/api/ip/myip") // 获取外网 IP
	if errClient != nil {
		log.Printf("獲取外部 IP 失敗，請檢查網路狀態\n")
		panic(errClient)
	}

	defer responseClient.Body.Close()

	body, _ := ioutil.ReadAll(responseClient.Body)
	clientIP := fmt.Sprintf("%s", string(body))
	return clientIP
}
