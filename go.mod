module github.com/deepch/RTSPtoWebRTC

go 1.15

require (
	github.com/deepch/vdk v0.0.0-20210115152623-8d167fd1c067
	github.com/gin-gonic/gin v1.6.3
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gorilla/websocket v1.4.2
)
