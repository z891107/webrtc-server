package main

import (
	"encoding/json"
	"log"
	"net/http"
	"sort"
	"sync"
	"time"

	"github.com/deepch/vdk/av"
	"github.com/gorilla/websocket"

	webrtc "github.com/deepch/vdk/format/webrtcv3"
	"github.com/gin-gonic/gin"
)

type Counter struct {
	sync.RWMutex
	val int
}

type JCodec struct {
	Type string
}

var webrtcConnectionCounter *Counter = new(Counter)

func (element *Counter) Add() {
	element.Lock()
	defer element.Unlock()
	element.val++
}

func (element *Counter) Sub() {
	element.Lock()
	defer element.Unlock()
	element.val--
}

func (element *Counter) GetValue() int {
	element.RLock()
	defer element.RUnlock()
	return element.val
}

func serveHTTP() {
	// Init Viewers
	Viewers = sync.Map{}

	router := gin.Default()
	router.LoadHTMLGlob("web/templates/*")
	router.GET("/", HTTPAPIServerIndex)
	router.GET("/stream/player/:uuid", HTTPAPIServerStreamPlayer)
	router.POST("/stream/receiver/:uuid", HTTPAPIServerStreamWebRTC)
	router.OPTIONS("/stream/receiver/:uuid", HTTPCORS)
	router.GET("/stream/codec/:uuid", HTTPAPIServerStreamCodec)
	router.OPTIONS("/stream/codec/:uuid", HTTPCORS)
	router.GET("/viewerlist", HTTPAPIServerWebsocketViewerList)
	router.GET("/serverlist", HTTPAPIServerWebsocketServerList)
	router.GET("/viewer", HTTPAPIServerViewer)

	router.StaticFS("/static", http.Dir("web/static"))
	err := router.Run(Config.Server.HTTPPort)
	if err != nil {
		log.Fatalln("Start HTTP Server error", err)
	}
}

func HTTPCORS(c *gin.Context) {
	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	c.Header("Access-Control-Allow-Headers", "Content-Type, X-Auth-Token, Origin, Authorization, Token")
}

//HTTPAPIServerWebsocket websocket
func HTTPAPIServerWebsocketServerList(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")

	upGrader := &websocket.Upgrader{
		//如果有 cross domain 的需求，可加入這個，不檢查 cross domain
		CheckOrigin: func(r *http.Request) bool { return true },
	}

	ws, err := upGrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		return
	}

	defer func() {
		log.Println("Websocket disconnect !!")
		ws.Close()
	}()

	for {

		mtype, _, err := ws.ReadMessage()
		if err != nil {
			log.Println("Websocket read:", err)
			break
		}

		err = ws.WriteMessage(mtype, GetByteServerList())
		if err != nil {
			log.Println("Websocket write:", err)
			break
		}
		time.Sleep(time.Second * 60)
	}

}

type ServerList struct {
	ServerList []string `json:"ServerList"`
}

func GetByteServerList() []byte {
	msg := Get_ServerList()
	resp, _ := json.Marshal(msg)
	return resp
}

//HTTPAPIServerWebsocketViewerList websocket
func HTTPAPIServerWebsocketViewerList(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")

	upGrader := &websocket.Upgrader{
		//如果有 cross domain 的需求，可加入這個，不檢查 cross domain
		CheckOrigin: func(r *http.Request) bool { return true },
	}

	ws, err := upGrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		return
	}

	defer func() {
		log.Println("Websocket disconnect !!")
		ws.Close()
	}()

	for {

		mtype, _, err := ws.ReadMessage()
		if err != nil {
			log.Println("Websocket read:", err)
			break
		}

		err = ws.WriteMessage(mtype, GetByteViewers())
		if err != nil {
			log.Println("Websocket write:", err)
			break
		}
	}

}

func GetByteViewers() []byte {
	msg := WebSocketGetViewers()
	resp, _ := json.Marshal(msg)
	return resp
}

//HTTPAPIServerViewer  viewer
func HTTPAPIServerViewer(c *gin.Context) {
	WriteAccessLog(c)
	c.HTML(http.StatusOK, "index.html", gin.H{
		"port":    Config.Server.HTTPPort,
		"version": time.Now().String(),
	})

}

//HTTPAPIServerIndex  index
func HTTPAPIServerIndex(c *gin.Context) {
	WriteAccessLog(c)
	_, all := Config.list()
	if len(all) > 0 {
		c.Header("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store")
		c.Redirect(http.StatusMovedPermanently, "stream/player/"+all[0])
	} else {
		c.HTML(http.StatusOK, "index.tmpl", gin.H{
			"port":    Config.Server.HTTPPort,
			"version": time.Now().String(),
		})
	}
}

//HTTPAPIServerStreamPlayer stream player
func HTTPAPIServerStreamPlayer(c *gin.Context) {
	WriteAccessLog(c)
	_, all := Config.list()
	sort.Strings(all)
	c.HTML(http.StatusOK, "player.tmpl", gin.H{
		"port":     Config.Server.HTTPPort,
		"suuid":    c.Param("uuid"),
		"suuidMap": all,
		"version":  time.Now().String(),
	})
}

//HTTPAPIServerStreamCodec stream codec
func HTTPAPIServerStreamCodec(c *gin.Context) {
	WriteAccessLog(c)
	c.Header("Access-Control-Allow-Origin", "*")

	// Check Token
	Token := c.Request.Header.Get("Token")
	if Get_String(Token) != 0 {
		c.String(http.StatusUnauthorized, "")
		return
	}

	if Config.ext(c.Param("uuid")) {
		Config.RunIFNotRun(c.Param("uuid"))
		codecs := Config.coGe(c.Param("uuid"))
		if codecs == nil {
			return
		}
		var tmpCodec []JCodec
		for _, codec := range codecs {
			if codec.Type() != av.H264 && codec.Type() != av.PCM_ALAW && codec.Type() != av.PCM_MULAW && codec.Type() != av.OPUS {
				log.Println("Codec Not Supported WebRTC ignore this track", codec.Type())
				continue
			}
			if codec.Type().IsVideo() {
				tmpCodec = append(tmpCodec, JCodec{Type: "video"})
			} else {
				tmpCodec = append(tmpCodec, JCodec{Type: "audio"})
			}
		}
		b, err := json.Marshal(tmpCodec)
		if err == nil {
			_, err = c.Writer.Write(b)
			if err != nil {
				log.Println("Write Codec Info error", err)
				return
			}
		}
	}
}

//HTTPAPIServerStreamWebRTC stream video over WebRTC
func HTTPAPIServerStreamWebRTC(c *gin.Context) {
	WriteAccessLog(c)
	c.Header("Access-Control-Allow-Origin", "*")

	// Check Token
	Token := c.Request.Header.Get("Token")
	if Get_String(Token) != 0 {
		c.String(http.StatusUnauthorized, "")
		return
	}
	Delete_String(Token)

	if !Config.ext(c.PostForm("suuid")) {
		log.Println("Stream Not Found")
		return
	}
	Config.RunIFNotRun(c.PostForm("suuid"))
	codecs := Config.coGe(c.PostForm("suuid"))
	if codecs == nil {
		log.Println("Stream Codec Not Found")
		return
	}
	var AudioOnly bool
	if len(codecs) == 1 && codecs[0].Type().IsAudio() {
		AudioOnly = true
	}
	muxerWebRTC := webrtc.NewMuxer(webrtc.Options{ICEServers: Config.GetICEServers(), PortMin: Config.GetWebRTCPortMin(), PortMax: Config.GetWebRTCPortMax()})
	answer, err := muxerWebRTC.WriteHeader(codecs, c.PostForm("data"))
	if err != nil {
		log.Println("WriteHeader", err)
		return
	}
	_, err = c.Writer.Write([]byte(answer))
	if err != nil {
		log.Println("Write", err)
		return
	}
	go func() {
		// Process log
		var tmp Viewer = Viewer{}
		tmp.RemoteAddr = c.Request.RemoteAddr
		tmp.Channel = c.PostForm("suuid")
		tmp.UserAgent = c.Request.UserAgent()
		tmp.StartTime = time.Now().In(cstZone).Unix()

		var SessionID int
		for SessionID = 0; SessionID < 10000; SessionID++ {
			_, ok := Viewers.LoadOrStore(SessionID, tmp)
			if !ok {
				break
			}
		}
		log.Print("RemoteAddr: ", tmp.RemoteAddr, " Channel: ", tmp.Channel, " UserAgent: ", tmp.UserAgent)
		defer WriteLeaveLog(tmp)
		defer Viewers.Delete(SessionID)

		webrtcConnectionCounter.Add()
		log.Println("!!!!!!!!!!!!!!!!!!!!!!!!Now Connection Num", webrtcConnectionCounter.GetValue())
		cid, ch := Config.clAd(c.PostForm("suuid"))
		defer Config.clDe(c.PostForm("suuid"), cid)
		defer muxerWebRTC.Close()
		defer webrtcConnectionCounter.Sub()
		defer log.Println("!!!!!!!!!!!!!!!!!!!!!!!!Now Connection Num", webrtcConnectionCounter.GetValue())
		var videoStart bool
		noVideo := time.NewTimer(10 * time.Second)
		for {
			select {
			case <-noVideo.C:
				log.Println("noVideo")
				return
			case pck := <-ch:
				if pck.IsKeyFrame || AudioOnly {
					noVideo.Reset(10 * time.Second)
					videoStart = true
				}
				if !videoStart && !AudioOnly {
					continue
				}
				err = muxerWebRTC.WritePacket(pck)
				if err != nil {
					log.Println("WritePacket", err)
					return
				}
			}
		}
	}()
}
