let stream = new MediaStream();

let suuid = $('#suuid').val();

let config = {
  iceServers: [{
    urls: ["stun:stun.l.google.com:19302"]
  }]
};

const pc = new RTCPeerConnection(config);
pc.onnegotiationneeded = handleNegotiationNeededEvent;

let log = msg => {
  document.getElementById('div').innerHTML += msg + '<br>'
}

pc.ontrack = function (event) {
  stream.addTrack(event.track);
  videoElem.srcObject = stream;
  log(event.streams.length + ' track is delivered')
}

pc.oniceconnectionstatechange = e => log(pc.iceConnectionState)

async function handleNegotiationNeededEvent() {
  let offer = await pc.createOffer();
  await pc.setLocalDescription(offer);
  getRemoteSdp();
}

var token
var apiToken = "9Q2v8T8MmIh1yxU0sTpy19Gt5hQIDKuhDBcX24uUAjjaocai7vWxXBTZSpTWbcpK"
function GetToken() {
  $.ajax({
    url: 'https://shake.streamtest.tk/login/',
    headers: {
      "Authorization": "Bearer " + apiToken 
    },
  }).then(function (response) {
    token = response.Token
    getCodecInfo()
  });
}

$(document).ready(function () {
  $('#' + suuid).addClass('active');
  GetToken()
});


function getCodecInfo() {
  $.ajax({
    url: "../codec/" + suuid,
    beforeSend: function (request) {
      request.setRequestHeader("Token", token);
    },
    type: 'GET',
  }).done(function (data) {
    try {
      data = JSON.parse(data);
    } catch (e) {
      console.log(e);
    } finally {
      $.each(data, function (index, value) {
        pc.addTransceiver(value.Type, {
          'direction': 'sendrecv'
        })
      })
      //send ping becouse PION not handle RTCSessionDescription.close()
      sendChannel = pc.createDataChannel('foo');
      sendChannel.onclose = () => console.log('sendChannel has closed');
      sendChannel.onopen = () => {
        console.log('sendChannel has opened');
        sendChannel.send('ping');
        setInterval(() => {
          sendChannel.send('ping');
        }, 1000)
      }
      sendChannel.onmessage = e => log(`Message from DataChannel '${sendChannel.label}' payload '${e.data}'`);
    }
  });
}

let sendChannel = null;

function getRemoteSdp() {
  $.ajax({
    url: "../receiver/" + suuid,
    beforeSend: function (request) {
      request.setRequestHeader("Token", token);
    },
    type: 'POST',
    data:
    {
      suuid: suuid,
      data: btoa(pc.localDescription.sdp)
    },
  }).done(function (data) {
    try {
      pc.setRemoteDescription(new RTCSessionDescription({
        type: 'answer',
        sdp: atob(data)
      }))
    } catch (e) {
      console.warn(e);
    }
  });
}